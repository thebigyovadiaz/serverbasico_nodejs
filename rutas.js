var rutas = {};
rutas['/'] = root;
rutas['/admin'] = admin;
rutas['/perfil'] = perfil;

function root(res){
	res.writeHead(200, {'Content-Type': 'text/html'});
	res.write('<h1 style="color:green;">Bienvenido, te encuentras en la pagina principal.</h1>');
	res.end();
}

function admin(res){
	res.writeHead(200, {'Content-Type': 'text/html'});
	res.write('<h1 style="color:blue;">Te encuentras en la pagina Admin.</h1>');
	res.end();
}

function perfil(res){
	res.writeHead(200, {'Content-Type': 'text/html'});
	res.write('<h1 style="color:purple;">Te encuentras en la pagina Perfil.</h1>');
	res.end();
}

module.exports.rutas = rutas;
