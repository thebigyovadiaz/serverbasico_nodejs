function router(rutas, url, res){
	if(typeof rutas[url] === 'function'){
		return rutas[url](res);
	} else {
		res.writeHead(404, {'Content-Type': 'text/html'});
		res.write('<h1 style="color:red;">Eror 404 - Pagina no encontrada</h1>');
		res.end();
	}
}

module.exports.router = router;
