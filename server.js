var http = require('http'),
	urls = require('url'),
	router = require('./router.js'),
	rutas = require('./rutas.js'),
	port = 3030;

http.createServer(function(req, res) {
	url = urls.parse(req.url).pathname;
	router.router(rutas.rutas, url, res);

}).listen(port, 'localhost', function(){
	console.log('El servidor se está ejecutando en el puerto ' + port);
});
