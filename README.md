# Servidor Básico con NodeJS

	Este servidor consta de tres archivos:

	* server.js: es donde se crea la aplicación del servidor, recibiendo http url y el puerto donde se quiere ejecutar dicho servidor, al igual se parsean uls y rutas para pruebas en el mismo.

	* rutas.js: es donde se crean las rutas y funciones que permitirán el acceso a dichas rutas y por ende mostrar su contenido.

	* router.js: es donde se verifica la existencia de la ruta colocada en la url sino muestra un error de página no encontrada.


## Ejecutar Servidor

	Ejecutar vía consola (terminal): node server.js

	Con el servidor en ejecución:
		* Accede a: localhost:3030/
		* Prueba ruta: localhost:3030/admin 
		* Prueba ruta: localhost:3030/perfil 
